import './App.css';
import {useState} from "react";
import Jugs from "./component/Jugs";

const App = () => {

    const [state, setState] = useState({})

    const handleChange = ({target}) => {

        setState({
            ...state,
            submitted: false,
            [target.name]: target.value
        });

    }

    const handleSubmit = (event) => {

        event.preventDefault();

        if (state.jugX && state.jugY && state.expected) {

            setState({...state, submitted: true});

        } else {

            alert('All fields are required.')

        }

    }

    return (
        <div className="App">

            <form onSubmit={handleSubmit}>

                <div className="App-header">

                    <label>{'Jug X'}</label>
                    <input type={'number'} pattern={'[0-9]*'} inputMode={'numeric'} name={'jugX'} value={state.jugX} onChange={handleChange}/>

                    <label>{'Jug Y'}</label>
                    <input type={'number'} pattern={'[0-9]*'} inputMode={'numeric'} name={'jugY'} value={state.jugY} onChange={handleChange}/>

                    <label>{'Expected'}</label>
                    <input type={'number'} pattern={'[0-9]*'} inputMode={'numeric'} name={'expected'} value={state.expected} onChange={handleChange}/>

                    <input type="submit" value="Submit"/>

                    {state.submitted && <Jugs state={state}/>}

                </div>

            </form>



        </div>
    );

}

export default App;
