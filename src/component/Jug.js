
const jugStyle = {
    borderStyle: 'solid',
    borderWidth: '0px 10px 10px 10px',
    borderRadius: '10px 10px 10px 10px',
    borderColor: 'aliceblue',
    height: 100,
    width: 100,
    marginTop: 100,
    marginLeft: 50,
    marginRight: 50,
    padding: 10,
    display: 'flex',
    flexDirection: 'column-reverse'

}

const jugContent = (jugSize, jug) => {

    return {
        bottom: 0,
        borderRadius: 10,
        backgroundColor: 'aliceblue',
        height: `${(jug/jugSize) * 100}%`
    };

}

const Jug = ({jugSize, jug, name}) => {

    return (

        <div>

            <div style={jugStyle}>

                <div style={jugContent(jugSize, jug)}/>

            </div>

            <div style={{display: 'flex', flexDirection: 'column'}}>
                <label>{name}</label>
                <label>{'Size ' + jugSize}</label>
                <label>{'Content: ' + jug}</label>
            </div>

        </div>

    );

}

export default Jug;
