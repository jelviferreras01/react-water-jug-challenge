import Jug from "./Jug";
import {useEffect, useState} from "react";

const Jugs = ({state}) => {

    const [jugX, setJugX] = useState(0);
    const [jugY, setJugY] = useState(0);
    const [solution, setSolution] = useState(true);

    useEffect(() => {

        (async () => await waterJugChallenge(+state.jugX, +state.jugY, +state.expected))();

    }, []);

    const waterJugChallenge = async (jugXSize, jugYSize, expected) => {

        let baseBucket, finalBucket, fillCount;

        let jugsSizes = [jugXSize, jugYSize],
            jugNames = ['jugX', 'jugY'],
            jugs = {jugX, jugY};

        if (
            jugsSizes.some((e) => e <= expected)
            && jugsSizes.some((e) => e >= expected)
            && jugsSizes.some((e) => !(expected % e))
        ) {

            baseBucket = !(expected % jugXSize) ? 0 : 1;
            finalBucket = +!baseBucket;
            fillCount = expected / jugsSizes[baseBucket];

            await delay(1000);

            while (fillCount > 0) {

                jugs[jugNames[baseBucket]] = jugsSizes[baseBucket];

                setJugX(jugs.jugX);
                setJugY(jugs.jugY);

                await delay(1500);

                if (jugs[jugNames[baseBucket]] !== expected) {

                    jugs[jugNames[finalBucket]] += jugs[jugNames[baseBucket]];
                    jugs[jugNames[baseBucket]] = 0;

                    setJugX(jugs.jugX);
                    setJugY(jugs.jugY);
                }

                await delay(1500)

                fillCount--
            }

        } else {

            setSolution(false);

        }

    }

    const delay = (ms) => new Promise(resolve => setTimeout(resolve, ms));

    return solution ? (

        <div style={{marginTop: 20}}>

            {[jugX, jugY].includes(+state.expected) && <label>{'Completed!'}</label>}

            <div style={{display: 'flex', flexDirection: 'row'}}>

                <Jug jugSize={state.jugX} jug={jugX} name={'Jug X'}/>

                <Jug jugSize={state.jugY} jug={jugY} name={'Jug Y'}/>

            </div>

        </div>

    ) : (

        <label>{'No solution'}</label>

    );

}

export default Jugs;
